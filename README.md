# Fortress

This repository hosts utilities for working with CCR New Fortress file formats.

- **Syroot.Fortress**: .NET Core library for accessing file formats.
- **Syroot.Fortress.Compressor**: Native rewrite of the compression method used by the game (s. below).
- **Syroot.Fortress.Scratchpad**: Small app to test the library with, currently extracts all sprites and gsysdata.dat.

## Compression

New Fortress uses Intel's 2001/2002 EFI compression method as documented in `res/intelefi.pdf`, appendices H and I.
Newer versions of this compression do not seem to work. CCR apparently copy-pasted this code into their `CCompress`
class, converting global variables to instance data and modifying it further to decompress in blocks. A rewrite of this
is found in `Syroot.Fortress.Compressor`, it exports the `Decompress` method for use by other libraries and apps.

## Scratchpad

This app currently searches for all `*.spr` files in a directory (which is specified in the source), and creates a
directory for each sprite file. It then converts each frame of it as a PNG file with the file name pattern
`{frame}_X{origin}_Y{origin}.{shareIndex}.png`.
