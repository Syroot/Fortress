﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Syroot.Fortress.Scratchpad
{
    internal static class Program
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private const string _inDir = @"C:\Games\NewFortress";
        private const string _outDir = @"C:\Games\NewFortress\_extract";

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main()
        {
            Directory.CreateDirectory(_outDir);
            ConvertGsysdata();
            ConvertSprites();
        }

        private static void ConvertGsysdata()
        {
            // Decompress gsysdata.dat text file.
            string inPath = Path.Combine(_inDir, "gsysdata.dat");
            Console.WriteLine($"Decompressing {inPath}...");

            using Stream stream = new FileStream(inPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            int decSize = stream.ReadInt32();
            byte[] decData = new byte[decSize];
            byte[] encData = new byte[stream.Length];
            stream.Read(encData);
            Compress.Decompress(encData, decData);

            string outPath = Path.Combine(_outDir, "gsysdata.txt");
            File.WriteAllBytes(outPath, decData);
        }

        private static void ConvertSprites()
        {
            // Decompress character sprites.
            foreach (string filename in Directory.EnumerateFiles(_inDir, "*.spr", SearchOption.AllDirectories))
            {
                Console.WriteLine($"Decompressing {filename}...");

                string outDir = Path.Combine(_outDir,
                    Path.GetDirectoryName(filename)![_inDir.Length..].TrimStart(Path.DirectorySeparatorChar),
                    Path.GetFileNameWithoutExtension(filename));
                Directory.CreateDirectory(outDir);

                using Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                SpriteFile file = new(stream);
                for (int i = 0; i < file.Sprites.Count; i++)
                {
                    Sprite sprite = file.Sprites[i];
                    SaveImage(sprite, Path.Combine(outDir,
                        $"{i}_X{sprite.BaseX}_Y{sprite.BaseY}.{sprite.ShareIndex}.png"));
                }
            }
        }

        private static unsafe void SaveImage(Sprite sprite, string outPath)
        {
            if (sprite.PixelFormat == D3DFormat.A1R5G5B5)
            {
                using Bitmap bitmap = new(sprite.Width, sprite.Height, PixelFormat.Format16bppArgb1555);
                BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    ImageLockMode.WriteOnly, bitmap.PixelFormat);

                for (int y = 0; y < sprite.Height; y++)
                {
                    Span<byte> dst = new((bitmapData.Scan0 + y * bitmapData.Stride).ToPointer(), bitmapData.Stride);
                    Span<byte> src = sprite.Data.AsSpan(y * sprite.Width * 2, sprite.Width * 2);
                    src.CopyTo(dst);
                }

                bitmap.UnlockBits(bitmapData);
                bitmap.Save(outPath, ImageFormat.Png);
            }
            else if (sprite.PixelFormat == D3DFormat.A4R4G4B4)
            {
                using Bitmap bitmap = new(sprite.Width, sprite.Height, PixelFormat.Format32bppArgb);
                BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    ImageLockMode.WriteOnly, bitmap.PixelFormat);

                for (int y = 0; y < sprite.Height; y++)
                {
                    Span<byte> dst = new((bitmapData.Scan0 + y * bitmapData.Stride).ToPointer(), bitmapData.Stride);
                    Span<byte> src = sprite.Data.AsSpan(y * sprite.Width * 2, sprite.Width * 2);
                    for (int x = 0; x < sprite.Width; x++)
                    {
                        dst[x * 4 + 0] = (byte)((src[x * 2 + 0] & 0b00001111) << 4);
                        dst[x * 4 + 1] = (byte)((src[x * 2 + 0] & 0b11110000) << 0);
                        dst[x * 4 + 2] = (byte)((src[x * 2 + 1] & 0b00001111) << 4);
                        dst[x * 4 + 3] = (byte)((src[x * 2 + 1] & 0b11110000) << 0);
                    }
                }

                bitmap.UnlockBits(bitmapData);
                bitmap.Save(outPath, ImageFormat.Png);
            }
            else
            {
                throw new NotImplementedException($"Unsupported pixel format {sprite.PixelFormat}.");
            }
        }
    }
}
