﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Fortress
{
    /// <summary>
    /// Represents the CCompress class which performs of LZ77 Huffman compression.
    /// </summary>
    public static unsafe class Compress
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public static void Decompress(ReadOnlySpan<byte> input, Span<byte> output)
        {
            fixed (void* pInput = input)
            fixed (void* pOutput = output)
            {
                Decompress(pOutput, pInput, (uint)input.Length, (uint)output.Length);
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [DllImport("compress")]
        private static extern void Decompress(void* dec, void* src, uint size, uint osize);
    }
}
