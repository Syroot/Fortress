﻿namespace Syroot.Fortress
{
    /// <summary>
    /// Represents known formats in which pixel color data of a <see cref="Sprite"/> is stored. This maps to the native
    /// D3DFORMAT enumeration.
    /// </summary>
    public enum D3DFormat
    {
        A1R5G5B5 = 25,
        A4R4G4B4 = 26
    }
}
