﻿using System;
using System.Buffers.Binary;
using System.IO;
using System.Text;

namespace Syroot.Fortress
{
    /// <summary>
    /// Represents extension method for <see cref="Stream"/> instances.
    /// </summary>
    public static class StreamExtensions
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public static string ReadAsciiString(this Stream stream, int length)
        {
            Span<byte> buffer = stackalloc byte[length];
            stream.Read(buffer);
            return Encoding.ASCII.GetString(buffer);
        }

        public static short ReadInt16(this Stream stream)
        {
            Span<byte> buffer = stackalloc byte[sizeof(short)];
            stream.Read(buffer);
            return BinaryPrimitives.ReadInt16LittleEndian(buffer);
        }

        public static int ReadInt32(this Stream stream)
        {
            Span<byte> buffer = stackalloc byte[sizeof(int)];
            stream.Read(buffer);
            return BinaryPrimitives.ReadInt32LittleEndian(buffer);
        }
    }
}
