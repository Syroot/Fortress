﻿using System.Collections.Generic;
using System.IO;

namespace Syroot.Fortress
{
    /// <summary>
    /// Represents an SPR <see cref="Sprite"/> container as listed in CID files.
    /// </summary>
    public class SpriteFile
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        public const string _header = "pLuS gAmE fOrMaT";

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public SpriteFile(Stream stream)
        {
            // Read header (not checked by game, not valid in some files).
            stream.ReadAsciiString(_header.Length);
            stream.Position += sizeof(int); // version?

            // Read sprite list.
            int spriteCount = stream.ReadInt32();
            while (spriteCount-- > 0)
            {
                Sprite sprite = new();
                sprite.Width = stream.ReadInt32();
                sprite.Height = stream.ReadInt32();
                sprite.BaseX = stream.ReadInt32();
                sprite.BaseY = stream.ReadInt32();
                sprite.PixelFormat = (D3DFormat)stream.ReadInt32();
                sprite.ShareIndex = stream.ReadInt16();
                stream.Position += 0xFE; // unused

                int origSize = stream.ReadInt32();
                int sourSize = stream.ReadInt32();
                sprite.Data = new byte[origSize];
                byte[] sourData = new byte[sourSize];
                stream.Read(sourData);
                Compress.Decompress(sourData, sprite.Data);

                Sprites.Add(sprite);
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IList<Sprite> Sprites { get; set; } = new List<Sprite>();
    }
}
