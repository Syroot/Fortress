﻿using System;

namespace Syroot.Fortress
{
    /// <summary>
    /// Represents the data stored for a CSprImg instance, which is a visual sprite in the game.
    /// </summary>
    public class Sprite
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int Width { get; set; }

        public int Height { get; set; }

        public int BaseX { get; set; }

        public int BaseY { get; set; }

        public D3DFormat PixelFormat { get; set; }

        public short ShareIndex { get; set; }

        public byte[] Data { get; set; } = Array.Empty<byte>();
    }
}
