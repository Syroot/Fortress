#pragma once
#include <stdint.h>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

extern "C" __declspec(dllexport) void Decompress(void* dec, void* src, uint32_t size, uint32_t osize);
