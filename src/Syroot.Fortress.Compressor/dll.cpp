#include "dll.h"
#include "CCompress.h"

void Decompress(void* dec, void* src, uint32_t size, uint32_t osize)
{
	CCompress compress;
	compress.Decode(dec, src, size, osize);
}
