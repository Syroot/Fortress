#pragma once
#include <stdint.h>

class CCompress
{
public:
	void Decode(void* dec, void* src, uint32_t size, uint32_t osize);

private:
	static constexpr uint32_t BITBUFSIZ = 16;
	static constexpr uint32_t MAXMATCH = 256;
	static constexpr uint32_t THRESHOLD = 3;
	static constexpr uint32_t CODE_BIT = 16;
	static constexpr uint32_t BAD_TABLE = -1;

	// C: Char&Len Set; P: Position Set; T: exTra Set
	static constexpr uint32_t NC = (0xFF + MAXMATCH + 2 - THRESHOLD);
	static constexpr uint32_t CBIT = 9;
	static constexpr uint32_t EFIPBIT = 4;
	static constexpr uint32_t MAXPBIT = 5;
	static constexpr uint32_t TBIT = 5;
	static constexpr uint32_t MAXNP = (1U << MAXPBIT) - 1;
	static constexpr uint32_t NT = CODE_BIT + 3;
	static constexpr uint32_t NPT = MAXNP;

	static uint16_t Pbit;

	uint8_t* Source; // Starting address of compressed data
	uint8_t* Dest; // Starting address of decompressed data
	uint32_t DestPos;
	uint32_t SourPos;
	uint32_t SourSize;

	uint16_t BitCount;
	uint16_t BitBuffer;
	uint16_t SubBitBuffer;
	uint16_t BlockSize;
	uint32_t CompSize;
	uint32_t OrigSize;

	uint16_t Left[2 * NC - 1];
	uint16_t Right[2 * NC - 1];
	uint8_t CLength[NC];
	uint8_t PTLength[NPT];
	uint16_t CTable[4096];
	uint16_t PTTable[256];

	void FillBuffer(uint16_t n);
	uint32_t GetBits(uint16_t n);

	uint16_t ReadPTLength(uint16_t nn, uint16_t nbit, uint16_t is);
	void ReadCLength();
	uint16_t DecodeC();
	uint32_t DecodeP();

	uint16_t MakeTable(uint16_t NumOfChar, uint8_t* BitLen, uint16_t TableBits, uint16_t* Table);

	void DecodeStart();
};
