#include "CCompress.h"
#include <cassert>

void CCompress::Decode(void* dec, void* src, uint32_t size, uint32_t osize)
{
	Source = (uint8_t*)src;
	OrigSize = osize;
	SourPos = 0;
	SourSize = size;
	DestPos = 0;
	Dest = (uint8_t*)dec;
	CompSize = size;
	BitBuffer = 0;
	SubBitBuffer = 0;
	BitCount = 0;
	FillBuffer(0x10);
	BlockSize = 0;

	DecodeStart();
}

uint16_t CCompress::Pbit = EFIPBIT;

void CCompress::FillBuffer(uint16_t n)
{
	BitBuffer = (uint16_t)(BitBuffer << n);
	while (n > BitCount)
	{
		BitBuffer |= (uint16_t)(SubBitBuffer << (n = (uint16_t)(n - BitCount)));
		if (CompSize > 0)
		{
			// Get 1 byte into SubBitBuf.
			CompSize--;
			SubBitBuffer = 0;
			SubBitBuffer = Source[SourPos++];
			BitCount = 8;
		}
		else
		{
			SubBitBuffer = 0;
			BitCount = 8;
		}
	}
	BitCount = (uint16_t)(BitCount - n);
	BitBuffer |= SubBitBuffer >> BitCount;
}

uint32_t CCompress::GetBits(uint16_t n)
{
	uint32_t OutBits = (uint32_t)(BitBuffer >> (BITBUFSIZ - n));
	FillBuffer(n);
	return OutBits;
}

uint16_t CCompress::ReadPTLength(uint16_t nn, uint16_t nbit, uint16_t is)
{
	uint16_t Number;
	uint16_t CharC;
	uint16_t Index;
	uint32_t Mask;

	assert(nn <= NPT);

	Number = (uint16_t)GetBits(nbit);

	if (Number == 0)
	{
		CharC = (uint16_t)GetBits(nbit);

		for (Index = 0; Index < 256; Index++)
			PTTable[Index] = CharC;

		for (Index = 0; Index < nn; Index++)
			PTLength[Index] = 0;

		return 0;
	}

	Index = 0;

	while (Index < Number && Index < NPT)
	{
		CharC = (uint16_t)(BitBuffer >> (BITBUFSIZ - 3));

		if (CharC == 7)
		{
			Mask = 1U << (BITBUFSIZ - 1 - 3);
			while (Mask & BitBuffer)
			{
				Mask >>= 1;
				CharC += 1;
			}
		}

		FillBuffer((uint16_t)((CharC < 7) ? 3 : CharC - 3));

		PTLength[Index++] = (uint8_t)CharC;

		if (Index == is)
		{
			CharC = (uint16_t)GetBits(2);
			CharC--;
			while ((int16_t)(CharC) >= 0 && Index < NPT)
			{
				PTLength[Index++] = 0;
				CharC--;
			}
		}
	}

	while (Index < nn && Index < NPT)
		PTLength[Index++] = 0;

	return MakeTable(nn, PTLength, 8, PTTable);
}

void CCompress::ReadCLength()
{
	uint16_t Number;
	uint16_t CharC;
	uint16_t Index;
	uint32_t Mask;

	Number = (uint16_t)GetBits(CBIT);

	if (Number == 0)
	{
		CharC = (uint16_t)GetBits(CBIT);

		for (Index = 0; Index < NC; Index++)
			CLength[Index] = 0;

		for (Index = 0; Index < 4096; Index++)
			CTable[Index] = CharC;

		return;
	}

	Index = 0;
	while (Index < Number)
	{
		CharC = PTTable[BitBuffer >> (BITBUFSIZ - 8)];
		if (CharC >= NT)
		{
			Mask = 1U << (BITBUFSIZ - 1 - 8);

			do
			{
				if (Mask & BitBuffer)
					CharC = Right[CharC];
				else
					CharC = Left[CharC];

				Mask >>= 1;

			} while (CharC >= NT);
		}
		// Advance what we have read.
		FillBuffer(PTLength[CharC]);

		if (CharC <= 2)
		{
			if (CharC == 0)
				CharC = 1;
			else if (CharC == 1)
				CharC = (uint16_t)(GetBits(4) + 3);
			else if (CharC == 2)
				CharC = (uint16_t)(GetBits(CBIT) + 20);

			CharC--;
			while ((int16_t)(CharC) >= 0)
			{
				CLength[Index++] = 0;
				CharC--;
			}
		}
		else
		{
			CLength[Index++] = (uint8_t)(CharC - 2);
		}
	}

	while (Index < NC)
		CLength[Index++] = 0;

	MakeTable(NC, CLength, 12, CTable);
}

uint16_t CCompress::DecodeC()
{
	uint16_t Index2;
	uint32_t Mask;

	if (BlockSize == 0)
	{
		// Starting a new block.
		BlockSize = (uint16_t)GetBits(16);
		ReadPTLength(NT, TBIT, 3);
		ReadCLength();
		ReadPTLength(MAXNP, Pbit, (uint16_t)(-1));
	}

	BlockSize--;
	Index2 = CTable[BitBuffer >> (BITBUFSIZ - 12)];

	if (Index2 >= NC)
	{
		Mask = 1U << (BITBUFSIZ - 1 - 12);

		do
		{
			if (BitBuffer & Mask)
				Index2 = Right[Index2];
			else
				Index2 = Left[Index2];

			Mask >>= 1;
		} while (Index2 >= NC);
	}
	// Advance what we have read.
	FillBuffer(CLength[Index2]);

	return Index2;
}

uint32_t CCompress::DecodeP()
{
	uint16_t Val;
	uint32_t Mask;
	uint32_t Pos;

	Val = PTTable[BitBuffer >> (BITBUFSIZ - 8)];

	if (Val >= MAXNP)
	{
		Mask = 1U << (BITBUFSIZ - 1 - 8);

		do
		{
			if (BitBuffer & Mask)
				Val = Right[Val];
			else
				Val = Left[Val];

			Mask >>= 1;
		} while (Val >= MAXNP);
	}
	// Advance what we have read.
	FillBuffer(PTLength[Val]);

	Pos = Val;
	if (Val > 1)
		Pos = (uint32_t)((1U << (Val - 1)) + GetBits((uint16_t)(Val - 1)));

	return Pos;
}

uint16_t CCompress::MakeTable(uint16_t NumOfChar, uint8_t* BitLen, uint16_t TableBits, uint16_t* Table)
{
	uint16_t Count[17];
	uint16_t Weight[17];
	uint16_t Start[18];
	uint16_t* Pointer;
	uint16_t Index3;
	uint16_t Index;
	uint16_t Len;
	uint16_t Char;
	uint16_t JuBits;
	uint16_t Avail;
	uint16_t NextCode;
	uint16_t Mask;
	uint16_t MaxTableLength;

	for (Index = 1; Index <= 16; Index++)
		Count[Index] = 0;

	for (Index = 0; Index < NumOfChar; Index++)
	{
		if (BitLen[Index] > 16)
			return (uint16_t)BAD_TABLE;
		Count[BitLen[Index]]++;
	}

	Start[1] = 0;

	for (Index = 1; Index <= 16; Index++)
		Start[Index + 1] = (uint16_t)(Start[Index] + (Count[Index] << (16 - Index)));

	/*(1U << 16)*/
	if (Start[17] != 0)
		return (uint16_t)BAD_TABLE;

	JuBits = (uint16_t)(16 - TableBits);

	for (Index = 1; Index <= TableBits; Index++)
	{
		Start[Index] >>= JuBits;
		Weight[Index] = (uint16_t)(1U << (TableBits - Index));
	}

	while (Index <= 16)
	{
		Weight[Index] = (uint16_t)(1U << (16 - Index));
		Index++;
	}

	Index = (uint16_t)(Start[TableBits + 1] >> JuBits);

	if (Index != 0)
	{
		Index3 = (uint16_t)(1U << TableBits);
		while (Index != Index3)
			Table[Index++] = 0;
	}

	Avail = NumOfChar;
	Mask = (uint16_t)(1U << (15 - TableBits));
	MaxTableLength = (uint16_t)(1U << TableBits);

	for (Char = 0; Char < NumOfChar; Char++)
	{
		Len = BitLen[Char];
		if (Len == 0 || Len >= 17)
			continue;

		NextCode = (uint16_t)(Start[Len] + Weight[Len]);

		if (Len <= TableBits)
		{
			if (Start[Len] >= NextCode || NextCode > MaxTableLength)
				return (uint16_t)BAD_TABLE;

			for (Index = Start[Len]; Index < NextCode; Index++)
				Table[Index] = Char;
		}
		else
		{
			Index3 = Start[Len];
			Pointer = &Table[Index3 >> JuBits];
			Index = (uint16_t)(Len - TableBits);

			while (Index != 0)
			{
				if (*Pointer == 0)
				{
					Right[Avail] = Left[Avail] = 0;
					*Pointer = Avail++;
				}

				if (Index3 & Mask)
					Pointer = &Right[*Pointer];
				else
					Pointer = &Left[*Pointer];

				Index3 <<= 1;
				Index--;
			}

			*Pointer = Char;
		}

		Start[Len] = NextCode;
	}

	// Succeeds.
	return 0;
}

void CCompress::DecodeStart()
{
	uint16_t BytesRemain;
	uint32_t DataIdx;
	uint16_t CharC;

	BytesRemain = (uint16_t)(-1);

	DataIdx = 0;

	for (;;)
	{
		CharC = DecodeC();

		if (CharC < 256)
		{
			// Process an Original character.
			Dest[DestPos++] = (uint8_t)CharC;
			if (DestPos >= OrigSize)
				return;
		}
		else
		{
			// Process a Pointer.
			CharC = (uint16_t)(CharC - (UINT8_MAX + 1 - THRESHOLD));

			BytesRemain = CharC;

			DataIdx = DestPos - DecodeP() - 1;

			BytesRemain--;
			while ((int16_t)(BytesRemain) >= 0)
			{
				if (DestPos >= OrigSize)
					return;
				if (DataIdx >= OrigSize)
					return;
				Dest[DestPos++] = Dest[DataIdx++];

				BytesRemain--;
			}
			// Once DestPos is fully filled, directly return.
			if (DestPos >= OrigSize)
				return;
		}
	}
}
